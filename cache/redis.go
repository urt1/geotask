package cache

import (
	"github.com/go-redis/redis"
)

func NewRedisClient(host, port string) *redis.Client {
	// реализуйте создание клиента для Redis
	res := redis.NewClient(&redis.Options{})
	return res
}
